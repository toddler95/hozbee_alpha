from __future__ import unicode_literals

from django.db import models

import hashlib,time
# Create your models here.

# Adresses
class Address(models.Model):
	address = models.AutoField(primary_key=True,unique=True,db_index=True)
	service_area = models.ForeignKey('sales.Area')
	pin = models.CharField(max_length=6)
	building = models.CharField(max_length=10)
	room = models.CharField(max_length=5)


# Customer Group
class CustomerGroup(models.Model):
	group = models.AutoField(primary_key=True,db_index=True)
	group_code = models.CharField(max_length=10)

# Customer details
class CustomerDetails(models.Model):
	customer = models.AutoField(primary_key=True,unique=True,db_index=True)
	firstname = models.CharField(max_length=20)
	lastname = models.CharField(max_length=20)
	email = models.EmailField(max_length=50)
	phone = models.CharField(max_length=10)
	dob = models.DateField()
	registration_date = models.DateField(auto_now_add=True)
	registration_time = models.TimeField(auto_now_add=True)
	active = models.BooleanField(default=True)
	group = models.ForeignKey(CustomerGroup)
	address = models.ForeignKey(Address)

	def __str__(self):
		return self.firstname + ' ' + self.lastname



# Customer Credentials	
class CustomerCredentials(models.Model):
	customer = models.OneToOneField(CustomerDetails,primary_key=True)
	username = models.CharField(max_length=50,db_index=True)
	password = models.CharField(max_length=64)

	# saving hashed password
	def save(self, *args, **kwargs):
		raw = str(self.password) + str(time.time())
		self.password = hashlib.sha224(raw).hexdigest()
		super(CustomerCredentials, self).save(*args, **kwargs)

























































