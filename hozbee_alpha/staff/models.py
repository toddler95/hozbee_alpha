from __future__ import unicode_literals

from django.db import models

# Create your models here.

# Delivery Boy

class DeliveryBoy(models.Model):
	deliveryboy = models.AutoField(primary_key=True,db_index=True)
	name = models.CharField(max_length=20)
	
