from __future__ import unicode_literals

from django.db import models

import hashlib,time

#Seller Details
class SellerDetails(models.Model):
	seller = models.AutoField(primary_key=True,db_index=True)
	firstname = models.CharField(max_length=20)
	lastname = models.CharField(max_length=20)
	phone1 = models.CharField(max_length=10)
	phone2 = models.CharField(max_length=10)
	email1 = models.EmailField(max_length=254)
	addressline1 = models.CharField(max_length=50)
	addressline2 = models.CharField(max_length=50)

# Seller Credentials
class SellerCredentials(models.Model):
	seller = models.OneToOneField('serviceproviders.SellerDetails',primary_key=True)
	username = models.CharField(max_length=50,db_index=True)
	password = models.CharField(max_length=64)

	# saving hashed password
	def save(self, *args, **kwargs):
		raw = str(self.password) + str(time.time())
		self.password = hashlib.sha224(raw).hexdigest()
		super(SellerCredentials, self).save(*args, **kwargs)

# Agreement Details
class Agreement(models.Model):
	agreement = models.AutoField(primary_key=True,db_index=True)
	legal_doc = models.CharField(max_length=20)
	seller = models.ForeignKey('SellerDetails')
	agreement_start_date = models.DateField(auto_now_add=True)
	agreement_end_date = models.DateField()

# Added Items
class AddedItems(models.Model):
	product = models.OneToOneField('products.UnitProduct')
	seller = models.ForeignKey('serviceproviders.SellerDetails')
	date = models.DateField(auto_now_add=True)
	time = models.TimeField(auto_now_add=True)

# Order Recieved
class RecievedOrder(models.Model):
	order = models.OneToOneField('sales.Order')
	product = models.ForeignKey('products.UnitProduct')
	quantity = models.PositiveSmallIntegerField()
	date = models.DateField(auto_now_add=True)
	time = models.TimeField(auto_now_add=True)
	status = models.CharField(max_length=2)